package com.zuitt.discussion.models;

import javax.persistence.*;

//marks this java object as a representation of an entity/record from the database table posts
@Entity
@Table(name = "posts")
public class Post {

    @Id //indicates the primary key
    @GeneratedValue //id will be auto-incremented
    private Long id;
    @Column
    private String title;
    @Column
    private String content;

    public Post() {
    }

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
