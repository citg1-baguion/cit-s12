package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping(value = "/users")
    public ResponseEntity<Object> createUser(@RequestBody User user){
        userService.createUser(user);
        return new ResponseEntity<>("User created successfully", HttpStatus.CREATED);
    }

    @GetMapping(value = "/users")
    public ResponseEntity<Object> getUsers(){
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    @PutMapping(value = "/users/{userid}")
    public ResponseEntity<Object> updateUser(@PathVariable Long userid,@RequestBody User user){
        return userService.updateUser(userid, user);
    }

    @DeleteMapping(value = "/users/{userid}")
    public ResponseEntity<Object> deleteUser(@PathVariable Long userid){
        return userService.deleteUser(userid);
    }
}
