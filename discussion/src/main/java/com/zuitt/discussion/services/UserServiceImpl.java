package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    public void createUser(User user){
        userRepository.save(user);
    }

    public Iterable<User> getUsers(){
        return userRepository.findAll();
    }

    public ResponseEntity updateUser(Long id, User user){
        User tempUser = userRepository.findById(id).get();

        tempUser.setUsername(user.getUsername());
        tempUser.setPassword((user.getPassword()));

        userRepository.save(tempUser);

        return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
    }

    public ResponseEntity deleteUser(Long id){
        userRepository.deleteById(id);
        return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
    }
}
